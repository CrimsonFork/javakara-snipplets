History is correct solutions to my informatics course.

Based on [JavaKara](https://www.swisseduc.ch/informatik/karatojava/javakara/).

*  [Aufgabe 1](https://gitlab.com/exe-cute/javakara-snipplets/commit/7c766552755cd9758fec98822f83d5817b4a658a)

*  [Aufgabe 2](https://gitlab.com/exe-cute/javakara-snipplets/commit/55338a2163f78365560fd586be853a7f43365589)

*  [Aufgabe 3-5](https://gitlab.com/exe-cute/javakara-snipplets/commit/3d209e544b7991df2924312c587fed112ae81b81)

*  [Aufgabe 6](https://gitlab.com/exe-cute/javakara-snipplets/commit/bd12dd38e801266f225f1e24bb8b945136b2bba3)

*  [Aufgabe 7](https://gitlab.com/exe-cute/javakara-snipplets/blob/master/FindeBaum) (Current release)

*exe-cute*